﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="PTI.Modules.CWB.View" %>

  <!--
  Loading Handsontable (full distribution that includes all dependencies apart from jQuery)
  -->
	<link data-jsfiddle="common" rel="stylesheet" media="screen" href="/desktopmodules/CWB/css/handsontable.min.css">
	<link data-jsfiddle="common" rel="stylesheet" media="screen" href="/desktopmodules/CWB/css/pikaday.css">
	<script data-jsfiddle="common" src="/desktopmodules/CWB/js/pikaday.js"></script>
	<script data-jsfiddle="common" src="/desktopmodules/CWB/js/moment.js"></script>
	<script data-jsfiddle="common" src="/desktopmodules/CWB/js/ZeroClipboard.js"></script>
	<script data-jsfiddle="common" src="/desktopmodules/CWB/js/handsontable.full.js"></script>

  <!--
  Loading demo dependencies. They are used here only to enhance the examples on this page
  -->
	<script src="/desktopmodules/CWB/js/highlight.pack.js"></script>
	<link rel="stylesheet" media="screen" href="/desktopmodules/CWB/css/github.css">
	<link rel="stylesheet" href="/desktopmodules/CWB/css/font-awesome.min.css">
	<link href="/desktopmodules/CWB/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/desktopmodules/CWB/css/style.css">
  
<div class="printThis">
<!-- Tab0 - Form List -->
<div id="tab0">
	<h1>Choose an existing Form or create a new one:</h1>
	<br />
	<span><b>New Form:</b> &nbsp; </span><input id="newName" class="searchButton" /><input id="btnCreate" type="button" onclick="createForm()" class="create_icon"></input>
	<br />
	<table id="formTable" class="tablestyle" style="width:625px;">
		<tr>
			<td class="tableheadTeal client-name">Name</td><!-- width: 36% -->
			<td class="tableheadTeal client-dates">Created</td><!-- width: 20% -->
			<td class="tableheadTeal client-dates">Last Updated</td><!-- width: 20% -->
			<td class="user-controls"></td><!-- width: 12% -->
			<td class="user-controls"></td><!-- width: 12% -->
		</tr>
	</table>
</div>
<!-- Tab2 - Pricing -->
<div id="tab1" class="printTabs" style="display:none">
<div class="navButtons doNotPrint">
	<div><img src="/desktopmodules/cwb/images/btn-pricing_ov.png" /></div>
	<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
	<div><a href="" onclick='showTab("2");return false;'><img src="/desktopmodules/cwb/images/btn-summary.png" /></a></div>
	<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
	<div><a href="" onclick='showTab("3");return false;'><img src="/desktopmodules/cwb/images/btn-discount.png" /></a></div>
	<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
	<div><a href="" onclick='showTab("4");return false;'><img src="/desktopmodules/cwb/images/btn-account.png" /></a></div>
	<div class="cwb-btn return-to-list"><a href="" onclick='showTab("0");return false;'>Return to List</a></div>
	<div class="cwb-btn return-to-list"><a onClick="makePrintable()">Print This Page</a></div>
</div>
	<h2 style="clear:both" class="accountNameH"></h2>
	<ul>
		<li>You MUST enter three months of data and ensure each field has a value entered (even if the value is zero).</li>
		<li>The Per Item Fees listed in are the standard fees listed on the Business Banking Charges Guide. </li>
	</ul>
<hr />
	<p style="clear:both;padding:15px 0px 15px 0px;" class="doNotPrint">
		<input id="searchButton" placeholder="Search" /><a name="searchbtn" id="searchbtn" onclick="javascript:return findAccount()" /><img src="/desktopmodules/cwb/images/search_icon.png" style="margin-top:-4px;margin-left:-7px;" /></a>
		<br />
	</p>
	<div id="wrapper">
		<div id="rowHeaders">
			<div id="accountsTable">
				<div class="darkGreyRow" colspan="3">Account Number</div>
				<div class="lightGreyRow" colspan="3">Per Item Fee</div>
				<div class="darkGreyRow" colspan="3">Minimum Monthly Fee</div>
				<div class="tealRow leftRow">Item</div>
				<div class="tealRow middleRow">Category</div>
				<div class="tealRow rightRow">Per Item Fee</div>
				<div colspan="3" style="height:25px; clear:both"></div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of Deposits</div>
					<div class="middleRow">Paper</div>
					<div class="rightRow">$0.90</div>
				</div>
				<div class="titleRow">
					<div class="leftRow" title="Per $1,000">$ of Bills Supplied</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$1.50</div>
				</div>
				<div class="titleRow alternateLightRow" title="Rolls of coin supplied (per 10 rolls)">
					<div class="leftRow"># of Rolls of Coin Supplied</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$3.50</div>
				</div>
				<div class="titleRow" title="Boxes of coin supplied (per box)">
					<div class="leftRow"># of Boxes of Coins Supplied</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$17.50</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of Cheques Deposited</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$0.18</div>
				</div>
				<div class="titleRow" title="$ of cash deposited (per $1000)">
					<div class="leftRow">$ of Cash Deposited</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$2.50</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of FBPS Deposits</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$7.00</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># of NDDS Deposits</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$4.50</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of NDDS Debit Adjustments</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$3.50</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># of NDDS Credit Adjustments</div>
					<div class="middleRow">Sundry</div>
					<div class="rightRow">$3.50</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of Cheques</div>
					<div class="middleRow">Paper</div>
					<div class="rightRow">$0.90</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># of EFT Credits</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of EFT Debits</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># of CWBdirect Txns</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># of IDP Txns</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow">
					<div class="leftRow">ABM Deposits</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow">ABM Withdrawls</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># of Cirrus ABM Withdrawls</div>
					<div class="middleRow">Electronic</div>
					<div class="rightRow">$0.75</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># Bank Drafts Purchased</div>
					<div class="middleRow">Paper</div>
					<div class="rightRow">$0.90</div>
				</div>
				<div class="titleRow">
					<div class="leftRow"># Utility Bills Paid</div>
					<div class="middleRow">Paper</div>
					<div class="rightRow">$0.90</div>
				</div>
				<div class="titleRow alternateLightRow">
					<div class="leftRow"># Cheques Certified</div>
					<div class="middleRow">Paper</div>
					<div class="rightRow">$0.90</div>
				</div>
			</div>
		</div>
		<div id="accounts" class="doNotPrint">
			<div id="header"><div class="newAccountWrapper doNotPrint" id="lastAccountColumn">
					<div></div>
					<div></div>
					<div></div>
					<div><a href="javascript:addAccount();" id="addAccount">Add Account</a></div>
				</div>
			</div>
			<div id="example1"></div>
		</div>
	</div>
	<div id="printableVersion" style="clear:both" class="onlyPrint"></div>
</div>

<!-- Tab2 - Summary -->
<div id="tab2" class="printTabs" style="display:none">
	<div class="navButtons doNotPrint">
		<div><a href="" onclick='showTab("1");return false;'><img src="/desktopmodules/cwb/images/btn-pricing.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><img src="/desktopmodules/cwb/images/btn-summary_ov.png" /></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a href="" onclick='showTab("3");return false;'><img src="/desktopmodules/cwb/images/btn-discount.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a href="" onclick='showTab("4");return false;'><img src="/desktopmodules/cwb/images/btn-account.png" /></a></div>
		<div class="cwb-btn return-to-list"><a href="" onclick='showTab("0");return false;'>Return to List</a></div>
		<div class="cwb-btn return-to-list"><a onClick="window.print()">Print This Page</a></div>
	</div>

	<h2 style="clear:both" class="accountNameH"></h2>
	<br />
	<table width="100%" id="summaryTable">
		<tbody>
			<tr>
				<td class="tableheadTeal">Account Number</td>
				<td class="tableheadTeal" style="border-left: 1px solid white;">Per Item Fee</td>
				<td class="tableheadTeal" style="border-left: 1px solid white;">Min. Monthly Fee</td>
				<td colspan="2" class="tableheadTeal" style="border-left: 1px solid white;">Paper Transactions</td>
				<td colspan="2" class="tableheadTeal" style="border-left: 1px solid white;">Electronic Transactions</td>
				<td colspan="2" class="tableheadTeal" style="border-left: 1px solid white;">Business Current Account Fees&nbsp;</td>
				<td colspan="2" class="tableheadTeal" style="border-left: 1px solid white;">Sundry Fees</td>
				<td colspan="2" class="tableheadTeal" style="border-left: 1px solid white;">Special Pricing Fees</td>
				<td class="tableheadTeal" style="border-left: 1px solid white;">$ Off Standard Fees Annually</td>
			</tr>
			<tr class="summaryHeaderDarkGrey">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Annual</td>
				<td>Monthly</td>
				<td>Annual</td>
				<td>Monthly</td>
				<td>Annual</td>
				<td>Monthly</td>
				<td>Annual</td>
				<td>Monthly</td>
				<td>Annual</td>
				<td>Monthly</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<p>&nbsp;</p>
	<hr/>
	<h1>Discount Summary</h1>
	<table class="tablestyle" style="float:left;margin-right:30px;margin-bottom:20px;">
		<tr>
			<td colspan="2" class="tableheadTeal">Total Service Fees Charged</td>
		</tr>
		<tr>
			<td>Annual</td>
			<td>Monthly</td>
		</tr>
		<tr>
			<td><label id="annualSummary"></label></td>
			<td><label id="monthlySummary"></label></td>
		</tr>
	</table>
	<table class="tablestyle" style="float:left;margin-right:30px;margin-bottom:20px;">
		<tr>
			<td class="tableheadTeal">Average Discount</td>
		</tr>
		<tr>
			<td>
				(-) Discount off Standard Fees<br/>
				(+) Markup on Standard Fees
			</td>
		</tr>
		<tr>
			<td><label id="summaryDiscount"></label></td>
		</tr>
	</table>
	<table class="tablestyle" style="float:left;">
		<tr>
			<td class="tableheadTeal">Average Dollars Off Per Account Annually</td>
		</tr>
		<tr>
			<td>
				(-) Discount off Standard Fees<br/>
				(+) Markup on Standard Fees
			</td>
		</tr>
		<tr>
			<td><label id="summaryAccount"></label></td>
		</tr>
	</table>
	<p style="clear:both;">Note: please ensure that you include comments in the Business Case or Annual Review Form (whichever is applicable) to spuport the pricing decision made above.</p>
	<p>&nbsp;</p>
	<hr />
	<h1>Approval(s) Required:</h1>
	<div id="BranchManager" style="display:none">
		<table>
			<tr>
				<td class="signature">Branch Manager Signature</td>
				<td class="gap"></td>
				<td class="signing-date">Date</td>
			</tr>
		</table>
	</div>
	<div id="RAVP" style="display:none">
		<table>
			<tr>
				<td class="signature">Regional AVP, Retail Banking Signature</td>
				<td class="gap"></td>
				<td class="signing-date">Date</td>
			</tr>
		</table>
	</div>
	<div id="RGM" style="display:none">
		<table>
			<tr>
				<td class="signature">Region SVP & GM Signature<br />
					Forward to Regional AVP, Retail Banking (RAVP). RAVP will coordinate approval with Regional SVP & GM.
				</td>
				<td class="gap"></td>
				<td class="signing-date">Date</td>
			</tr>
		</table>
	</div>
</div>
<!-- Tab3 - Discount -->
<div id="tab3" class="printTabs" style="display:none">
	<div class="navButtons doNotPrint">
		<div><a href="" onclick='showTab("1");return false;'><img src="/desktopmodules/cwb/images/btn-pricing.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a href="" onclick='showTab("2");return false;'><img src="/desktopmodules/cwb/images/btn-summary.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><img src="/desktopmodules/cwb/images/btn-discount_ov.png" /></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a href="" onclick='showTab("4");return false;'><img src="/desktopmodules/cwb/images/btn-account.png" /></a></div>
		<div class="cwb-btn return-to-list"><a href="" onclick='showTab("0");return false;'>Return to List</a></div>
		<div class="cwb-btn return-to-list"><a onClick="window.print()">Print This Page</a></div>
	</div>

	<h2 style="clear:both" class="accountNameH"></h2>
	<br />
	<table style="width:100%;" class="serviceTable">
		<tbody>
			<tr>
				<td class="tableheadTeal">Service Charge Summary</td>
				<td class="tableheadTeal" style="border-left: 1px solid white;font-weight:normal;">Annual</td>
				<td class="tableheadTeal" style="border-left: 1px solid white;font-weight:normal;">Monthly</td>
			</tr>
			<tr class="darkGreyRow">
				<td style="border-left: 1px solid white;">Number Of Accounts</td>
				<td colspan="2" class="alternateLightRow"><label id="chargeAccounts"></label></td>
			</tr>
			<tr class="lightGreyRow">
				<td style="border-left: 1px solid white;">Total Standard Fees</td>
				<td class="alternateWhiteRow"><label id="totalStdFeesAnn"></label></td>
				<td class="alternateWhiteRow"><label id="totalStdFeesMth" class="alternateWhiteRow"></label></td>
			</tr>
			<tr class="darkGreyRow">
				<td style="border-left: 1px solid white;">Total Proposed Fees</td>
				<td class="alternateLightRow"><label id="totalProFeesAnn"></label></td>
				<td class="alternateLightRow"><label id="totalProFeesMth"></label></td>
			</tr>
			<tr class="lightGreyRow">
				<td style="border-left: 1px solid white;">Average $'s Off Per Account</td>
				<td class="alternateWhiteRow"><label id="avgOffAnn"></label></td>
				<td class="alternateWhiteRow"><label id="avgOffMth"></label></td>
			</tr>
			<tr class="darkGreyRow">
				<td style="border-left: 1px solid white;">Discount</td>
				<td colspan="2" class="alternateLightRow"><label id="chargeDiscount"></label></td>
			</tr>
		</tbody>
	</table>
	<p>&nbsp;</p>
	<table style="width:100%;" class="serviceTable">
		<tbody>
			<tr>
				<td class="tableheadTeal" style="border-left: 1px solid white;" colspan="2">Discount Calculator</td>
			</tr>
			<tr class="goldRow">
				<td>Desired Discount</td>
				<td><input id="desiredDiscount" onchange="discountCalc()" class="desiredDiscount" placeholder="Enter Discount %" onkeypress="return isNumberKey(event)" /></td>
			</tr>
			<tr class="darkGreyRow">
				<td>Service Charges (Annually)</td>
				<td class="lightBlueRow"><label id="serviceChargeAnn"></label></td>
			</tr>
			<tr class="lightGreyRow">
				<td>Service Charges (Monthly)</td>
				<td class="lightBlueRow"><label id="serviceChargeMth"></label></td>
			</tr>
		</tbody>
	</table>
</div>
<!-- Tab4 - Account Selector -->
<div id="tab4" class="printTabs" style="display:none">
		<div class="navButtons doNotPrint">
		<div><a onclick='showTab("1");return false;' href=""><img src="/desktopmodules/cwb/images/btn-pricing.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a onclick='showTab("2");return false;' href=""><img src="/desktopmodules/cwb/images/btn-summary.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><a onclick='showTab("3");return false;' href=""><img src="/desktopmodules/cwb/images/btn-discount.png" /></a></div>
		<div><img src="/desktopmodules/cwb/images/bar.png" /></div>
		<div><img src="/desktopmodules/cwb/images/btn-account_ov.png" /></div>
		<div class="cwb-btn return-to-list"><a href="" onclick='showTab("0");return false;'>Return to List</a></div>
		<div class="cwb-btn return-to-list"><a onClick="window.print()">Print This Page</a></div>
	</div>
		<p>
		<h2 style="clear:both" class="accountNameH"></h2>
		<b>Date: </b><label id="date"></label> &nbsp; &nbsp; <span class="doNotPrint"><b>Account:</b> <select id="selectAccounts" onchange="accountChange()"></select></span>
	</p>
	<div id="accountDetails" style="display:none">
		<table class="serviceTable" style="width:48%;float:left">
			<tbody>
				<tr>
					<td colspan="2" class="tableheadTeal">Operating Account</td>
				</tr>
				<tr>
					<td class="darkGreyRow">Average Monthly Balance</td>
					<td class="alternateLightRow"><input id="serviceAveMthBal" placeholder="Enter Value" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow">Minimum Monthly Balance</td>
					<td><input id="serviceMinMthBal" placeholder="Enter Value" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td>&nbsp</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2" class="tableheadTeal">Monthly Transaction Activity</td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of Deposits</td>
					<td class="alternateLightRow"><input id="serviceDeposits" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of Cheques Deposited</td>
					<td><input id="serviceCheques" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">Monthly Cash Deposited</td>
					<td class="alternateLightRow"><input id="serviceCashDep" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of Cheques Written</td>
					<td><input id="serviceChequesWritten" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of EFT Credits</td>
					<td class="alternateLightRow"><input id="serviceEFTCredit" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of EFT Debits</td>
					<td><input id="serviceEFTDebits" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of CWBdirect Transactions</td>
					<td class="alternateLightRow"><input id="serviceCWB" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of Interac Direct Transactions</td>
					<td><input id="serviceInterac" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of ABM Deposits</td>
					<td class="alternateLightRow"><input id="serviceABMDeposits" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of ABM Withdrawls</td>
					<td><input id="serviceABMWith" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of Cirrus ABM Withdrawls</td>
					<td class="alternateLightRow"><input id="serviceCirrusABMWith" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of Bank Drafts Purchased</td>
					<td><input id="serviceDraft" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow"># of Utilty Bills Paid</td>
					<td class="alternateLightRow"><input id="serviceBills" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"># of Cheques Certified</td>
					<td><input id="serviceCert" onchange="accountChange();return false;" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr class="lightBlueRow">
					<td>Total Paper:</td>
					<td><label id="serviceTotalPaper"></label></td>
				</tr>
				<tr class="lightBlueRow">
					<td>Total Electronic:</td>
					<td><label id="serviceTotalElec"></label></td>
				</tr>
				<tr class="lightBlueRow">
					<td>GRAND TOTAL:</td>
					<td><label id="serviceTotal"></label></td>
				</tr>
			</tbody>
		</table>
		<table class="serviceTable" style="width:48%; float:right;">
			<tbody>
				<tr>
					<td colspan="2" class="tableheadTeal">Bundling (Check for Yes)</td>
				</tr>
				<tr>
					<td class="darkGreyRow">Business Saving Account</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceSavings" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"><em>Average Monthly Balance</em></td>
					<td><input id="serviceAveBal" placeholder="Enter Value" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">CWBdirect Business</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceCWBBusiness" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow">CWBdirect</td>
					<td><input type="checkbox" id="serviceCWBdirect" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">CAFT</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceCAFT" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow">Night Deposit</td>
					<td><input type="checkbox" id="serviceNightly" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">Business Vista Gold Card</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceVisa" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"><em># of Business Visa Gold Cards</em></td>
					<td><input id="serviceNumVisa" placeholder="Enter Value" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">Coverdraft</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceCoverdraft" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow"><em># of Coverdraft Transactions</em></td>
					<td><input id="serviceNumCoverdraft" placeholder="Enter Value" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">Moneris</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceMoneris" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="lightGreyRow">Ceridian</td>
					<td><input type="checkbox" id="serviceCeridian" onchange="accountChange();return false;" /></td>
				</tr>
				<tr>
					<td class="darkGreyRow">National Direct Deposit Service (NDDS)</td>
					<td class="alternateLightRow"><input type="checkbox" id="serviceNDDS" onchange="accountChange();return false;" /></td>
				</tr>
			</tbody>
		</table>
		<p>&nbsp;</p>
		<table style="width:100%;" class="serviceTable">
			<tbody>
				<tr>
					<td class="tableheadTeal"></td>
					<td class="tableheadTeal" style="border-left: 1px solid white;">Business Current Account</td>
					<td class="tableheadTeal" style="border-left: 1px solid white;">Business Current Account PLUS</td>
					<td class="tableheadTeal" style="border-left: 1px solid white;">CMA - Standard</td>
					<td class="tableheadTeal" style="border-left: 1px solid white;">CMA - Large</td>
					<td class="tableheadTeal" style="border-left: 1px solid white;">Business Saving Account</td>
				</tr>
				<tr class="alternateLightRow">
					<td class="darkGreyRow">Monthly Fee</td>
					<td><label id="Service1-1"></label></td>
					<td><label id="Service1-2"></label></td>
					<td><label id="Service1-3"></label></td>
					<td><label id="Service1-4"></label></td>
					<td><label id="Service1-5"></label></td>
				</tr>
				<tr>
					<td class="lightGreyRow">Additional Paper Fees</td>
					<td><label id="Service2-1"></label></td>
					<td><label id="Service2-2"></label></td>
					<td><label id="Service2-3"></label></td>
					<td><label id="Service2-4"></label></td>
					<td><label id="Service2-5"></label></td>
				</tr>
				<tr class="alternateLightRow">
					<td class="darkGreyRow" style="line-height:20px;">Additional Electronic Fees</td>
					<td><label id="Service3-1"></label></td>
					<td><label id="Service3-2"></label></td>
					<td><label id="Service3-3"></label></td>
					<td><label id="Service3-4"></label></td>
					<td><label id="Service3-5"></label></td>
				</tr>
				<tr>
					<td class="lightGreyRow">Average Monthly Fee</td>
					<td><label id="Service4-1"></label></td>
					<td><label id="Service4-2"></label></td>
					<td><label id="Service4-3"></label></td>
					<td><label id="Service4-4"></label></td>
					<td><label id="Service4-5"></label></td>
				</tr>
				<tr class="alternateLightRow">
					<td class="darkGreyRow" style="line-height:20px;">Bundling Fees (-) / <br>Savings (+)</td>
					<td><label id="Service5-1"></label></td>
					<td><label id="Service5-2"></label></td>
					<td><label id="Service5-3"></label></td>
					<td><label id="Service5-4"></label></td>
					<td><label id="Service5-5"></label></td>
				</tr>
				<tr>
					<td class="lightBlueRow" style="line-height:18px;">Net Total <br><span style="font-size:11px;">Fees (-) OR Intrest (+)</span></td>
					<td class="lightBlueRow"><label id="Service6-1"></label></td>
					<td class="lightBlueRow"><label id="Service6-2"></label></td>
					<td class="lightBlueRow"><label id="Service6-3"></label></td>
					<td class="lightBlueRow"><label id="Service6-4"></label></td>
					<td><label id="Service6-5"></label></td>
				</tr>
				<tr>
					<td class="lightBlueRow" style="line-height:18px;">Value<br><span style="font-size:11px;">(Bundling Saving &amp; Interest Revenue)</span></td>
					<td class="lightBlueRow"><label id="Service7-1"></label></td>
					<td class="lightBlueRow"><label id="Service7-2"></label></td>
					<td class="lightBlueRow"><label id="Service7-3"></label></td>
					<td class="lightBlueRow"><label id="Service7-4"></label></td>
					<td><label id="Service7-5"></label></td>
				</tr>
			</tbody>
		</table>

		<table class="finalTable">
			<tr>
				<td class="goldRow" style="border-bottom:1px solid white;">Best Account for Client:</td>
				<td class="goldRow" style="border-left:1px solid white;border-bottom:1px solid white;">
					<label id="ServiceBestAccount"></label>
					<label id="ServiceBestAccount2"></label>
				</td>
			</tr>
			<tr>
				<td class="goldRow" style="border-bottom:1px solid white;">Average Monthly Fee:</td>
				<td class="goldRow" style="border-left:1px solid white;border-bottom:1px solid white;"><label id="ServiceMthFee"></label></td>
			</tr>
		</table>
	</div>
	<div id="accountSelect">
		Select an account to see more details.
	</div>
</div>
<!-- Templates for rows and columns -->
<div style="display:none;" id="template"><div class="accountHeaderWrapper">
	<div style="position:relative">
		<input id="account" class="accountName" placeholder="Account Name" onchange="saveAccount(this)" />
		<a onclick="deleteAccount(this);return false;" href="" class="doNotPrint"><img src="/desktopmodules/cwb/images/delete.png" style="position:absolute;top:0px;right:0px;width:20px;height:20px" /></a>
	</div>
	<div style="position:relative">
		<input id="perItem" class="currency" onchange="saveAccount(this)" title="Per Item Fee: Use the Set icon to use standard fee. (i.e. Bus. Curr. Pricing)" />
		<a onclick="setStandardPrice(this);return false;" href="" class="doNotPrint"><img src="/desktopmodules/cwb/images/icon-std.png" style="position:absolute;top: 15px;right:5px;width:36px;height:13px" /></a>
	</div>
	<div style="position:relative">
		<input id="minMonth" class="currency" onchange="saveAccount(this)" title="Minimum Monthly Fee: Use the Set icon to use standard fee. (i.e. Bus. Curr. Pricing)" />
		<a onclick="setStandardPrice(this);return false;" href="" class="doNotPrint"><img src="/desktopmodules/cwb/images/icon-std.png" style="position:absolute;top:15px;right:5px;width:36px;height:13px" /></a>
	</div>
	<div>Month</div>
	<div>
		<div>1</div><div>2</div><div>3</div>
	</div><input id="hdnID" style="display:none" />
</div><table>
		<tr class="accountRow">
			<td><label id="sumAccountNum"></label></td>
			<td><label id="sumPerItem" style="display:none"></label><label id="sumPerItemDisplay"></label></td>
			<td><label id="sumMinMonth" style="display:none"></label><label id="sumMinMonthDisplay"></label></td>
			<td><label id="paperAnnual"></label></td>
			<td><label id="paperMonthly"></label></td>
			<td><label id="elecAnnual"></label></td>
			<td><label id="elecMonthly"></label></td>
			<td><label id="busAnnual"></label></td>
			<td><label id="busMonthly"></label></td>
			<td><label id="sundryAnnual"></label></td>
			<td><label id="sundryMonthly"></label></td>
			<td><label id="specialAnnual"></label></td>
			<td><label id="specialMonthly"></label></td>
			<td><label id="standardFee"></label></td>
		</tr>
	</table><table>
		<tr class="formRow">
			<td>
				<input id="editName" style="display:none" />
				<a href="" onclick='showForm(this);return false;'><label id="formName" class="form-name"></label></a><label id="hdnFormId" style="display:none"></label>
				<div class="edit-controls">
				<a id="editSave" class="edit-btn" href="" onclick="updateForm(this);return false;" style="display:none">Save</a>
				<a id="editCancel" class="edit-btn" href="" onclick="updateCancel(this);return false;" style="display:none">Cancel</a>
				</div>
			</td>
			<td><label id="formCreatedDate"></label></td>
			<td><label id="formUpdatedDate"></label></td>
			<td><a href="" onclick="editForm(this);return false;">Edit</a></td>
			<td><a href="" onclick="deleteForm(this);return false;">Delete</a></td>
		</tr>
	</table>
</div>
	<div id="printableVersion"></div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/desktopmodules/CWB/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="/desktopmodules/CWB/js/jquery.maskMoney.js" type="text/javascript"></script>
<script src="/desktopmodules/CWB/js/page.js" type="text/javascript"></script>

<script>
	var formId = <%= !string.IsNullOrEmpty(Request.QueryString["formId"])?Request.QueryString["formId"].ToString():"1" %>;
	var moduleId = <%= ModuleId.ToString() %>;

//Document Load
	$(document).ready(function(){
		loadFormList();
		$('body').children().addClass('doNotPrint');
		showItem($('.printThis'));
		window.onbeforeprint = function() {
			loadSummary();
		};
		$('body').on('keydown', 'input, select, textarea', function(e) {
			var self = $(this)
			  , form = self.parents('form:eq(0)')
			  , focusable
			  , next
			;
			if (e.keyCode == 13) {
				focusable = form.find('input,a,select,button,textarea').filter(':visible');
				next = focusable.eq(focusable.index(this)+1);
				if (next.length) {
					next.focus();
				}
				return false;
			}
		});
	});
</script>