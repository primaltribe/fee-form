﻿
//TAB0 FUNCTIONS
//prep form row
function insertNewFormRow() {
	var newDiv = $("#template").find(".formRow").clone(true);
	$("#formTable tr:last").after(newDiv);
	return newDiv;
}

//Inital form list load
function loadFormList() {
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/GetForms',
		data: JSON.stringify({
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = JSON.parse(msg.d);

			data.forEach(function (form) {
				var newDiv = insertNewFormRow();
				$(newDiv).find("#formName").text(form.FormName);
				$(newDiv).find("#formUpdatedDate").text(formatDate(form.LastModifiedOnDate));
				$(newDiv).find("#formCreatedDate").text(formatDate(form.CreatedOnDate));
				$(newDiv).find("#hdnFormId").text(form.FormId);
			});
		}
	});
}

//load the selected form
function showForm(context) {
	formId = $(context.parentElement.parentElement).find("#hdnFormId").text();
	LoadForm();
	showTab(1);
	$('#tab4').find('#serviceAveMthBal').maskMoney({ allowZero: true });
	$('#tab4').find('#serviceMinMthBal').maskMoney({ allowZero: true });
	$('#tab4').find('#serviceAveBal').maskMoney({ allowZero: true });
}

//load the selected form
function editForm(context) {
	//Get Name
	var namelabel = $(context.parentElement.parentElement).find("#formName");
	var formName = $(namelabel).text();
	//Set Hide Label
	$(namelabel).hide();
	//get edit input
	var nameControl = $(context.parentElement.parentElement).find("#editName");
	//set edit to name and show
	$(nameControl).val(formName);
	$(nameControl).show();
	//show edit button
	$(context.parentElement.parentElement).find("#editSave").show();
	$(context.parentElement.parentElement).find("#editCancel").show();
}

function updateForm(context) {
	var formName = $(context.parentElement.parentElement).find("#editName").val();
	formId = $(context.parentElement.parentElement).find("#hdnFormId").text();
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/UpdateForm',
		data: JSON.stringify({
			formIdStr: "\"" + formId + "\"",
			formNameStr: "\"" + formName + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			//Hide button
			$(context).hide();
			var container = $(context.parentElement.parentElement);
			//Hide input and cancel button
			$(container).find("#editName").hide();
			$(container).find("#editCancel").hide();
			//Set text and Show Label
			$(container).find("#formName").text(formName);
			$(container).find("#formName").show();
		}
	});
}

function updateCancel(context) {
	//Hide button
	$(context).hide();
	var container = $(context.parentElement.parentElement);
	//Hide input and cancel button
	$(container).find("#editName").hide();
	$(container).find("#editSave").hide();
	//Show lable
	$(container).find("#formName").show();
}

function deleteForm(context) {
	if (confirm('Are you sure you would like to permanently delete this form ?')) {
		formId = $(context.parentElement.parentElement).find("#hdnFormId").text();
		$.ajax({
			type: "POST",
			url: '/desktopmodules/CWB/FeeFormService.asmx/DeleteForm',
			data: JSON.stringify({
				formIdStr: "\"" + formId + "\""
			}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (msg) {
				$(context.parentElement.parentElement).remove();
			}
		});
	}
}

//Create a new form
function createForm() {
	var formName = $("#newName").val();
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/CreateForm',
		data: JSON.stringify({
			formNameStr: "\"" + formName + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = JSON.parse(msg.d);
			formId = data;
			LoadForm();
			showTab(1);
		}
	});
}

//TAB1 FUNCTIONS
//grid set up
var hot;
function setupGrid() {
	hot = new Handsontable(document.getElementById("example1"), {
		startRows: 21,
		startCols: 3,
		rowHeaders: false,
		colHeaders: false,
		minSpareRows: 0,
		contextMenu: false,
		viewportColumnRenderingOffset: 900,
		type: 'numeric',
		format: "0",
		afterChange: function (change, source) {
			if (source === 'loadData') {
				return; //don't save this change
			}

			$.ajax({
				type: "POST",
				url: '/desktopmodules/CWB/FeeFormService.asmx/SaveMutipleGridChanges',
				data: JSON.stringify({
					changesStr: "\"" + change + "\"",
					formIdStr: "\"" + formId + "\""
				}),
				contentType: "application/json; charset=utf-8",
				dataType: "json"
			});
		}
	});
}

function LoadForm() {
	setupGrid();
	loadAccounts();
	$("#accountsTable").tooltip();
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/GetForm',
		data: JSON.stringify({
			formIdStr: "\"" + formId + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = JSON.parse(msg.d);
			$(".accountNameH").each(function (index) {
				$(this).html(data.FormName);
			});
		}
	});
}

//load Accounts when one is selected from tab0
function loadAccounts() {
	//Load the header account items
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/GetAccounts',
		data: JSON.stringify({
			formIdStr: "\"" + formId + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = jQuery.parseJSON(msg.d);

			data.forEach(function (entry) {
				var newDiv = insertNewAccount();
				$(newDiv).find("#hdnID").val(entry.AccountId);
				$(newDiv).find("#account").val(entry.AccountName);
				if (entry.AccountPerItemFee >= 0)
					$(newDiv).find("#perItem").val(round(entry.AccountPerItemFee, 2));
				else
					$(newDiv).find("#perItem").val("");

				if (entry.AccountMinMonthFee >= 0)
					$(newDiv).find("#minMonth").val(round(entry.AccountMinMonthFee, 2));
				else
					$(newDiv).find("#minMonth").val("");

				$(newDiv).tooltip();
				$(newDiv).find('.currency').maskMoney({ allowZero: true });
			});
		}
	});
	//Load the Account Grid
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/GetEntries',
		data: JSON.stringify({
			formIdStr: "\"" + formId + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = JSON.parse(msg.d);
			hot.loadData(data.data);
		}
	});
}

//Prep the new account column(s)
function insertNewAccount() {
	var newDiv = document.getElementById("template").firstChild.cloneNode(true);
	document.getElementById("header").insertBefore(newDiv, document.getElementById("lastAccountColumn"));
	return newDiv;
}
//Add account when the button is pressed
function addAccount() {
	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/CreateAccount',
		data: JSON.stringify({
			formIdStr: "\"" + formId + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = jQuery.parseJSON(msg.d);

			var newDiv = insertNewAccount();
			$(newDiv).find("#hdnID").val(data);
			$(newDiv).tooltip();
			$(newDiv).find('.currency').maskMoney({allowZero:true});
			$(newDiv).find('.currency').each(function () {
				$(this).val("");
			});
			hot.alter('insert_col', null, 3);

			//Scroll to position of the new account
			var elem = document.getElementById('accounts');
			elem.scrollLeft = elem.scrollWidth;
		}
	});
}

//Save changes in the accountnum/itemfee/minmonthfee are changed
function saveAccount(context) {
	var accountId = $(context.parentElement.parentElement).find("#hdnID").val();
	var name = $(context.parentElement.parentElement).find("#account").val();
	var perItem = $(context.parentElement.parentElement).find("#perItem").val();
	var minMonth = $(context.parentElement.parentElement).find("#minMonth").val();

	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/SaveAccount',
		data: JSON.stringify({
			accountIdStr: "\"" + accountId + "\"",
			nameStr: "\"" + name + "\"",
			perItemStr: "\"" + perItem + "\"",
			minMonthStr: "\"" + minMonth + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			var data = jQuery.parseJSON(msg.d);
		}
	});
}

function deleteAccount(context) {
	if (confirm('Would you like to continue and permanently delete this account?')) {
		var accountId = $(context.parentElement.parentElement).find("#hdnID").val();
		$.ajax({
			type: "POST",
			url: '/desktopmodules/CWB/FeeFormService.asmx/DeleteAccount',
			data: JSON.stringify({
				accountIdStr: "\"" + accountId + "\""
			}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (msg) {
				var data = jQuery.parseJSON(msg.d);

				var accountCount = 0;
				$("#accounts").find(".accountHeaderWrapper").each(function (index) {
					if (this == context.parentElement.parentElement) {
						//delete rows from grid. index*3 - index*3+3
						hot.alter('remove_col', accountCount * 3, 3);
					}
					accountCount++;
				});
				$(context.parentElement.parentElement).remove();
			}
		});
	}
	return false;
}

function setStandardPrice(context) {
	var control = $(context.previousSibling.previousSibling).val("Standard Fee");
}

//(search) Find and scroll to account
function findAccount() {
	var term = document.getElementById("searchButton").value.toLowerCase();
	if (term == "")
		return false;

	$(".accountName").each(function () {
		if ($(this).val().toLowerCase().indexOf(term) >= 0) {
			var offset = $(this).offset().left;
			offset = offset - $("#account").offset().left + $(this).width();
			$('#accounts').animate({
				scrollLeft: offset - 149
			}, 2000);
			return false;
		}
	})
	return false;
}

//TAB2 Functions
//preset Per Item and default fees
var STDFlatFee = 5;
var STDPaperFee = 0.9;
var STDElecFee = 0.75;
var depositsFee = 0.9;
var chequesFee = 0.9;
var eftCredits = 0.75;
var eftDebits = 0.75;
var cwbDirect = 0.75;
var IDP = 0.75;
var abmDeposits = 0.75;
var abmWithdrawal = 0.75;
var cirrusFee = 0.75;
var bankDraft = 0.9;
var utility = 0.9;
var chequestCert = 0.9;
var billsFee = 1.5;
var rolls = 3.5;
var boxes = 17.5;
var chequesDeposit = 0.18;
var cashDeposit = 2.5;
var FBPSFee = 7;
var nddsDeposited = 4.5;
var nddsDebit = 3.5;
var nddsCredit = 3.5;

//Prep Summary Account Rows
function insertNewSummaryAccount() {
	var newDiv = $("#template").find(".accountRow").clone(true);
	$("#summaryTable tr:last").after(newDiv);
	return newDiv;
}

var sundryTotal;
var STDFeeTotal;

function loadSummary() {
	//Totals for columns
	var dollarsOffTotal = 0;
	sundryTotal = 0;
	STDFeeTotal = 0;
	var accountCount = 0;
	var specialTotal = 0;
	var accountCount = 0;

	//Delete existing rows
	$("#summaryTable").find("tr:gt(1)").remove()
	$('#selectAccounts')
		.find('option')
		.remove()
		.end()
		.append('<option value="-1">Select Account</option>')
		.val('-1');

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();

	$("#date").text(formatDate(today));
	$("#accounts").find(".accountHeaderWrapper").each(function (index) {
		accountCount++;
		var newDiv = insertNewSummaryAccount();

		//Add to Account selector Select
		$("#selectAccounts").append($("<option></option>")
			.attr("value", $(this).find("#hdnID").val())
			.text($(this).find("#account").val()));

		$(newDiv).find("#sumAccountNum").text($(this).find("#account").val());
		var perItemFee = $(this).find("#perItem").val();
		if (!isNumber(perItemFee))
			perItemFee = -1;
		$(newDiv).find("#sumPerItem").text(-1);
		if (perItemFee == -1)
			$(newDiv).find("#sumPerItemDisplay").text("STD");
		else
			$(newDiv).find("#sumPerItemDisplay").text("$" + round(perItemFee, 2));

		var FlatFee = $(this).find("#minMonth").val();
		if (!isNumber(FlatFee))
			FlatFee = -1;
		if (FlatFee == -1)
			$(newDiv).find("#sumMinMonthDisplay").text("STD");
		else
			$(newDiv).find("#sumMinMonthDisplay").text("$" + round(FlatFee, 2));
		$(newDiv).find("#sumMinMonth").text(FlatFee);


		var col1 = hot.getDataAtCol(index * 3);
		var col2 = hot.getDataAtCol((index * 3) + 1);
		var col3 = hot.getDataAtCol((index * 3) + 2);

		var TotalPaper = col1[0] + col2[0] + col3[0] + col1[10] + col2[10] + col3[10] + col1[18] + col2[18] + col3[18];
		TotalPaper += +col1[19] + col2[19] + col3[19] + col1[20] + col2[20] + col3[20];
		TotalPaper = TotalPaper * 4;

		var TotalElec = col1[11] + col2[11] + col3[11] + col1[12] + col2[12] + col3[12] + col1[13] + col2[13] + col3[13];
		TotalElec += col1[14] + col2[14] + col3[14] + col1[15] + col2[15] + col3[15];
		TotalElec += col1[16] + col2[16] + col3[16] + col1[17] + col2[17] + col3[17];
		TotalElec = TotalElec * 4;

		var business = 0;
		var deposits = col1[0] + col2[0] + col3[0];
		var cheques = col1[10] + col2[10] + col3[10];
		var EFTCR = col1[11] + col2[11] + col3[11];
		var EFTDR = col1[12] + col2[12] + col3[12];
		var CWBdirect = col1[13] + col2[13] + col3[13];
		var IDP = col1[14] + col2[14] + col3[14];
		var ABMCR = col1[15] + col2[15] + col3[15];
		var ABMDR = col1[16] + col2[16] + col3[16];
		var Cirrus = col1[17] + col2[17] + col3[17];
		var BD = col1[18] + col2[18] + col3[18];
		var UB = col1[19] + col2[19] + col3[19];
		var CC = col1[20] + col2[20] + col3[20];
		var busVal = (((deposits * depositsFee) + (cheques * chequesFee) + (EFTCR * eftCredits) + (EFTDR * eftDebits) + (CWBdirect * cwbDirect) + (IDP * abmDeposits) + (ABMCR * abmDeposits) + (ABMDR * abmWithdrawal) + (Cirrus * cirrusFee) + (BD * bankDraft) + (UB * utility) + (CC * chequestCert))) * 4;
		if (busVal >= STDFlatFee * 12) {
			business = busVal;
		} else {
			business = STDFlatFee * 12;
		}

		var sundry = 0;
		var bills = col1[1] + col2[1] + col3[1];
		var rollscoin = col1[2] + col2[2] + col3[2];
		var boxcoin = col1[3] + col2[3] + col3[3];
		var chequesdep = col1[4] + col2[4] + col3[4];
		var cash = col1[5] + col2[5] + col3[5];
		var fbps = col1[6] + col2[6] + col3[6];
		var ndds = col1[7] + col2[7] + col3[7];
		var nddsDR = col1[8] + col2[8] + col3[8];
		var nddsCR = col1[9] + col2[9] + col3[9];

		sundry = (((bills / 1000) * billsFee) + ((rollscoin / 10) * rolls) + (boxcoin * boxes) + (chequesdep * chequesDeposit) + ((cash / 1000) * cashDeposit) + (fbps * FBPSFee) + (ndds * nddsDeposited) + (nddsDR * nddsDebit) + (nddsCR * nddsCredit)) * 4
		var sundryMonthly = sundry / 12

		if (sundryMonthly < 100) {
			sundry = 0
			sundryMonthly = 0
		}

		var STDFees = business;
		var SpecialFees = 0;

		//STD per item and STD flat fee (THIS IS WORKING)
		if (perItemFee === -1 && FlatFee === -1) {
			SpecialFees = STDFees;
		}

			//STD per item and non-STD flat fee (This is working)
		else if (perItemFee == -1 && FlatFee >= 0) {
			SpecialFees = (((TotalPaper * STDPaperFee)) + (TotalElec * STDElecFee)) / 12
			if (SpecialFees > FlatFee) {
				SpecialFees = SpecialFees * 12
			} else {
				SpecialFees = FlatFee * 12
			}
		}

			//non-STD per item fee and STD flat fee (THIS IS WORKING)
		else if (perItemFee >= 0 && FlatFee == -1) {
			SpecialFees = (((TotalPaper + TotalElec) * perItemFee) / 12)
			if (SpecialFees >= STDFlatFee) {
				SpecialFees = SpecialFees * 12
			} else {
				SpecialFees = STDFlatFee * 12
			}
		}

			//non-STD per item fee and non-STD flat fee (THIS IS WORKING)
		else if (perItemFee >= 0 && FlatFee >= 0) {
			SpecialFees = ((TotalPaper + TotalElec)) * (perItemFee)
			if (SpecialFees >= (FlatFee * 12)) {
				SpecialFees = SpecialFees
			} else {
				SpecialFees = (FlatFee * 12)
			}
		}

		var standard = 0;
		standard = 0 - ((sundry + business) - SpecialFees);

		dollarsOffTotal += standard;
		sundryTotal += sundry;
		STDFeeTotal += business;
		specialTotal += SpecialFees;

		//Set grid row values
		$(newDiv).find("#paperAnnual").text(round(TotalPaper, 0));
		$(newDiv).find("#paperMonthly").text(round(TotalPaper / 12, 0));
		$(newDiv).find("#elecAnnual").text(round(TotalElec, 0));
		$(newDiv).find("#elecMonthly").text(round(TotalElec / 12, 0));
		$(newDiv).find("#busAnnual").text("$" + round(business, 2));
		$(newDiv).find("#busMonthly").text("$" + round(business / 12, 2));
		$(newDiv).find("#sundryAnnual").text("$" + round(sundry, 2));
		$(newDiv).find("#sundryMonthly").text("$" + round(sundryMonthly, 2));
		$(newDiv).find("#specialAnnual").text("$" + round(SpecialFees, 2));
		$(newDiv).find("#specialMonthly").text("$" + round(SpecialFees / 12, 2));
		$(newDiv).find("#standardFee").text("$" + round(standard, 2));
	});

	var avDiscount = (dollarsOffTotal / (sundryTotal + STDFeeTotal)) * 100;
	var avDollarsOff = dollarsOffTotal / accountCount;

	//Approvals
	//$("#BranchManager").hide();
	$("#RAVP").hide();
	$("#RGM").hide();
	if (avDiscount >= -15 || avDollarsOff >= -125) {
		//Branch Manager
		$("#BranchManager").show();
	} else if ((avDiscount >= -30 && avDiscount < -15) || (avDollarsOff >= -200 && avDollarsOff < -125)) {
		//RAVP
		$("#RAVP").show();
	} else {
		//RGM
		$("#RGM").show();
	}

	//Set Discount Summary values
	$("#annualSummary").text("$" + round(specialTotal, 2));
	$("#monthlySummary").text("$" + round(specialTotal / 12, 2));
	$("#summaryDiscount").text(round(avDiscount, 2) + "%");
	$("#summaryAccount").text("$" + round(avDollarsOff, 2));

	//Discount Tab

	$("#chargeAccounts").text(accountCount);
	$("#totalStdFeesAnn").text("$" + round(sundryTotal + STDFeeTotal, 2));
	$("#totalStdFeesMth").text("$" + round((sundryTotal + STDFeeTotal) / 12, 2));
	$("#totalProFeesAnn").text("$" + round(specialTotal, 2));
	$("#totalProFeesMth").text("$" + round(specialTotal / 12, 2));
	$("#avgOffAnn").text("$" + round(avDollarsOff, 2));
	$("#avgOffMth").text("$" + round(avDollarsOff / 12, 2));
	$("#chargeDiscount").text(round(avDiscount, 2) + "%");
}

//Tab3 Functions
function discountCalc() {
	var charge = $("#desiredDiscount").val();
	if (!isNumber(charge))
		return;
	var discount = (100 - charge) / 100;
	$("#serviceChargeAnn").text("$" + round((sundryTotal + STDFeeTotal) * discount, 2));
	$("#serviceChargeMth").text("$" + round(((sundryTotal + STDFeeTotal) / 12) * discount, 2));
}
function accountChange() {
	accountId = $("#selectAccounts").val();
	//Skip if nothing is selected
	if (accountId == "-1") {
		$("#accountDetails").hide();
		$("#accountSelect").show();
		return;
	}



	$.ajax({
		type: "POST",
		url: '/desktopmodules/CWB/FeeFormService.asmx/GetEntry',
		data: JSON.stringify({
			accountIdStr: "\"" + accountId + "\""
		}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			//set up display
			$("#accountDetails").show();
			$("#accountSelect").hide();

			var data = jQuery.parseJSON(msg.d);
			var paperTotal = 0;
			var elecTotal = 0;
			var AddPaperFee = 0;
			var AddElecFee = 0;
			var temp;
			temp = (data.Deposits1 + data.Deposits2 + data.Deposits3) / 3;
			paperTotal += temp;
			AddPaperFee += temp * 1;
			$("#serviceDeposits").val(round(temp, 0));
			$("#serviceCheques").val(round((data.ChequesDeposited1 + data.ChequesDeposited2 + data.ChequesDeposited3) / 3, 0));
			$("#serviceCashDep").val(round((data.Cash1 + data.Cash2 + data.Cash3) / 3, 0));
			temp = (data.Cheques1 + data.Cheques2 + data.Cheques3) / 3;
			paperTotal += temp;
			AddPaperFee += temp * 5;
			$("#serviceChequesWritten").val(round(temp, 0));
			temp = (data.EFTCredits1 + data.EFTCredits2 + data.EFTCredits3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 1;
			$("#serviceEFTCredit").val(round(temp, 0));
			temp = (data.EFTDebits1 + data.EFTDebits2 + data.EFTDebits3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 5;
			$("#serviceEFTDebits").val(round(temp, 0));
			temp = (data.CWB1 + data.CWB2 + data.CWB3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 5;
			$("#serviceCWB").val(round(temp, 0));
			temp = (data.IDP1 + data.IDP2 + data.IDP3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 5;
			$("#serviceInterac").val(round(temp, 0));
			temp = (data.ABMDeposits1 + data.ABMDeposits2 + data.ABMDeposits3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 1;
			$("#serviceABMDeposits").val(round(temp, 0));
			temp = (data.ABMWithdrawls1 + data.ABMWithdrawls2 + data.ABMWithdrawls3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 5;
			$("#serviceABMWith").val(round(temp, 0));
			temp = (data.Cirrus1 + data.Cirrus2 + data.Cirrus3) / 3;
			elecTotal += temp;
			AddElecFee += temp * 5;
			$("#serviceCirrusABMWith").val(round(temp, 0));
			temp = (data.Drafts1 + data.Drafts2 + data.Drafts3) / 3;
			paperTotal += temp;
			AddPaperFee += temp * 5;
			$("#serviceDraft").val(round(temp, 0));
			temp = (data.Utility1 + data.Utility2 + data.Utility3) / 3;
			paperTotal += temp;
			AddPaperFee += temp * 5;
			$("#serviceBills").val(round(temp, 0));
			temp = (data.ChequesCertified1 + data.ChequesCertified2 + data.ChequesCertified3) / 3;
			paperTotal += temp;
			AddPaperFee += temp * 5;
			$("#serviceCert").val(round(temp, 0));

			$("#serviceTotalPaper").text(round(paperTotal, 0));
			$("#serviceTotalElec").text(round(elecTotal, 0));
			$("#serviceTotal").text(round(paperTotal + elecTotal, 0));

			//Column 1
			var busCurrSubTotal = 0;
			if ((paperTotal * .9) + (elecTotal * .75) < 5) {
				busCurrSubTotal = 5 - ((paperTotal * .9) + (elecTotal * .75));
				$("#Service1-1").text("$" + round(busCurrSubTotal, 2));
			}
			else
				$("#Service1-1").text("$" + round(0, 2));
			$("#Service2-1").text("$" + round(paperTotal * .9, 2));
			$("#Service3-1").text("$" + round(elecTotal * .75, 2));
			$("#Service4-1").text("$" + round(-((paperTotal * .9) + (elecTotal * .75) + busCurrSubTotal), 2));
			var service5_1 = ($("serviceCWBBusiness").checked ? -25 : 0) + ($("serviceCWBdirect").checked ? -10 : 0) + ($("serviceCAFT").checked ? (-75 / 12) : 0) + ($("serviceNightly").checked ? (-24 / 12) : 0) + ($("serviceVisa").checked ? ($("serviceNumVisa").val() * (-25 / 12)) : 0) + ($("serviceCoverdraft").checked ? ($("serviceNumCoverdraft").val() * 0.5) : 0);
			$("#Service5-1").text("$" + round(-service5_1, 2));
			$("#Service6-1").text("$" + round(-((paperTotal * .9) + (elecTotal * .75) + busCurrSubTotal) + service5_1, 2));
			$("#Service7-1").text("$" + round(busCurrSubTotal, 2));

			//Column2
			var busCurrPlusSubTotal = 0;
			if ((paperTotal * .9) + (elecTotal * .75) < 9) {
				busCurrPlusSubTotal = 9 - ((paperTotal * .9) + (elecTotal * .75));
				$("#Service1-2").text("$" + round(busCurrPlusSubTotal, 2));
			}
			else
				$("#Service1-2").text("$" + round(0, 2));
			$("#Service2-2").text("$" + round(paperTotal * .9, 2));
			$("#Service3-2").text("$" + round(elecTotal * .75, 2));
			$("#Service4-2").text("$" + round(-((paperTotal * .9) + (elecTotal * .75) + busCurrPlusSubTotal), 2));
			var service5_2 = ($("serviceCWBBusiness").checked ? 25 : 0) + ($("serviceCWBdirect").checked ? 10 : 0) + ($("serviceCAFT").checked ? (75 / 12) : 0) + ($("serviceNightly").checked ? (24 / 12) : 0) + ($("serviceVisa").checked ? ($("serviceNumVisa").val() * (25 / 12)) : 0) + ($("serviceCoverdraft").checked ? ($("serviceNumCoverdraft").val() * 0.5) : 0);
			$("#Service5-2").text("$" + round(-service5_1, 2));
			$("#Service6-2").text("$" + round(-((paperTotal * .9) + (elecTotal * .75) + busCurrPlusSubTotal) + service5_2, 2));
			$("#Service7-2").text("$" + round(busCurrSubTotal, 2));

			//Column3
			$("#Service1-3").text("$" + round(40, 2));
			var cmaSubTotal = 0;
			var tempval = (paperTotal + elecTotal) > 50 ? ((((paperTotal + elecTotal) - 50) * .6) * .9) : 0;
			cmaSubTotal += tempval;
			$("#Service2-3").text("$" + round(tempval, 2));
			tempval = (paperTotal + elecTotal) > 50 ? ((((paperTotal + elecTotal) - 50) * .4) * .75) : 0;
			cmaSubTotal += tempval;
			$("#Service3-3").text("$" + round(tempval, 2));
			$("#Service4-3").text("$" + round(-(cmaSubTotal + 40), 2));
			var service5_3 = ($("serviceCWBBusiness").checked ? 25 : 0) + ($("serviceCWBdirect").checked ? 10 : 0) + ($("serviceCAFT").checked ? (75 / 12) : 0) + ($("serviceNightly").checked ? (24 / 12) : 0) + ($("serviceVisa").checked ? ($("serviceNumVisa").val() * (25 / 12)) : 0) + ($("serviceCoverdraft").checked ? ($("serviceNumCoverdraft").val() * 0.5) : 0);
			$("#Service5-3").text("$" + round(-service5_1, 2));
			$("#Service6-3").text("$" + round(-(cmaSubTotal + 40), 2));
			$("#Service7-3").text("$" + round(-service5_1, 2));

			//Column4
			$("#Service1-4").text("$" + round(75, 2));
			var cmaLargeSubTotal = 0;
			tempval = (paperTotal + elecTotal) > 100 ? ((((paperTotal + elecTotal) - 100) * .6) * .85) : 0;
			cmaLargeSubTotal += tempval;
			$("#Service2-4").text("$" + round(tempval, 2));
			tempval = (paperTotal + elecTotal) > 100 ? ((((paperTotal + elecTotal) - 100) * .4) * .7) : 0;
			cmaLargeSubTotal += tempval
			$("#Service3-4").text("$" + round(tempval, 2));
			$("#Service4-4").text("$" + round(-(cmaLargeSubTotal + 75), 2));
			var service5_4 = ($("serviceCWBBusiness").checked ? 25 : 0) + ($("serviceCWBdirect").checked ? 10 : 0) + ($("serviceCAFT").checked ? (75 / 12) : 0) + ($("serviceNightly").checked ? (24 / 12) : 0) + ($("serviceVisa").checked ? ($("serviceNumVisa").val() * (25 / 12)) : 0) + ($("serviceCoverdraft").checked ? ($("serviceNumCoverdraft").val() * 0.5) : 0);
			$("#Service5-4").text("$" + round(-service5_1, 2));
			$("#Service6-4").text("$" + round(-(cmaLargeSubTotal + 75), 2));
			$("#Service7-4").text("$" + round(-service5_1, 2));

			//column5
			$("#Service1-5").text("$" + round(0, 2));
			$("#Service2-5").text("$" + round(AddPaperFee, 2));
			$("#Service3-5").text("$" + round(AddElecFee, 2));
			$("#Service4-5").text("$" + round(AddPaperFee + AddElecFee, 2));
			$("#Service5-5").text("$" + round(service5_1, 2));

			var maxNet = Math.max(-((paperTotal * .9) + (elecTotal * .75) + busCurrSubTotal + service5_1), -((paperTotal * .9) + (elecTotal * .75) + busCurrPlusSubTotal) + service5_2, -(cmaSubTotal + 40), -(cmaLargeSubTotal + 75));

			$("#ServiceBestAccount").text( (maxNet == -((paperTotal * .9) + (elecTotal * .75) + busCurrSubTotal) + service5_1 ? "Business Current Account" : (maxNet == -(cmaSubTotal + 40) ? "CMA - Standard" : (maxNet == -((paperTotal * .9) + (elecTotal * .75) + busCurrPlusSubTotal) + service5_2 ? "Business Current Account PLUS" : "CMA - Large"))));
			$("#ServiceBestAccount2").text( ($("#serviceAveBal").val() > 0 < 0 ? "& Business Savings Account" : ""));
			$("#ServiceMthFee").text("$" + ($("#ServiceBestAccount").text() == "Business Current Account" ? round(-((paperTotal * .9) + (elecTotal * .75) + busCurrSubTotal), 2) : ($("#ServiceBestAccount").text() == "CMA - Standard" ? round(-(cmaSubTotal + 40), 2) : ($("#ServiceBestAccount").text() == "Business Current Account PLUS" ? round(-((paperTotal * .9) + (elecTotal * .75) + busCurrPlusSubTotal), 2) : round(-(cmaLargeSubTotal + 75), 2)))));
		}
	});
}

//Util Functions
function showTab(tab) {
	if (tab == "0") {
		if (confirm('Are you sure you would like to return to the main page?')) {
			location.reload();
		}
		else
			return false;
	}
	else if (tab == "1") {
		$("#tab0").hide();
		$("#tab1").show();
		$("#tab2").hide();
		$("#tab3").hide();
		$("#tab4").hide();
	}
	else if (tab == "2") {
		loadSummary();
		$("#tab0").hide();
		$("#tab1").hide();
		$("#tab2").show();
		$("#tab3").hide();
		$("#tab4").hide();
	}
	else if (tab == "3") {
		loadSummary();
		$("#tab0").hide();
		$("#tab1").hide();
		$("#tab2").hide();
		$("#tab3").show();
		$("#tab4").hide();
	}
	else if (tab == "4") {
		loadSummary();
		$("#tab0").hide();
		$("#tab1").hide();
		$("#tab2").hide();
		$("#tab3").hide();
		$("#tab4").show();
	}
}

function round(value, decimals) {
	return ((Number(Math.round(value + 'e' + decimals) + 'e-' + decimals)).toFixed(decimals)).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

function isNumber(n) {
	n = n.replace(",", "");
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function formatDate(_date) {
	_date = new Date(_date);
	var dd = _date.getDate();
	var mm = _date.getMonth() + 1;
	var yyyy = _date.getFullYear();

	return dd + '/' + mm + '/' + yyyy;
}
function showItem($node) {
	$node.removeClass('doNotPrint');
	$node.siblings().addClass('doNotPrint')
	if ($node.parent().length) {
		showItem($node.parent());
	}
}
/*
		 * HTML: Print Wide HTML Tables
		 * http://salman-w.blogspot.com/2013/04/printing-wide-html-tables.html
		 */
function makePrintable() {
	$("#printableVersion").empty();
	var table = $("#accounts")
	table.attr('class', '');
	var tableWidth = 0;
	table.find(".accountHeaderWrapper").each(function () {
		tableWidth += $(this).width();
		tableWidth += 1; //account for margin and padding
	});

	var tableHeight = table.outerHeight(),
		pageWidth = 1050,
		pageCount = Math.ceil(tableWidth / pageWidth),
		printWrap = $("<div></div>").appendTo($("#printableVersion")),
		i,
		printPage;

	for (i = 0; i < pageCount; i++) {
		printPage = $("<div></div>").css({
			"overflow": "hidden",
			"width": pageWidth,
			"page-break-before": i === 0 ? "auto" : "always",
			"height": tableHeight,
			"padding-top": "1px",
		}).appendTo(printWrap);
		table.clone().removeAttr("id").appendTo(printPage).css({
			"position": "relative",
			"left": -i * pageWidth,
			"width": tableWidth
		});
	}
	table.attr('class', 'doNotPrint');
	//table.hide();
	window.print()
}