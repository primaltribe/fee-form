﻿using System;
using DotNetNuke.Entities.Users;
using PTI.Modules.CWB.Components;
using DotNetNuke.Services.Exceptions;

namespace PTI.Modules.CWB
{
	/// -----------------------------------------------------------------------------
	/// <summary>   
	/// The Edit class is used to manage content
	/// 
	/// Typically your edit control would be used to create new content, or edit existing content within your module.
	/// The ControlKey for this control is "Edit", and is defined in the manifest (.dnn) file.
	/// 
	/// Because the control inherits from CWBModuleBase you have access to any custom properties
	/// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
	/// 
	/// </summary>
	/// -----------------------------------------------------------------------------
	public partial class Edit : CWBModuleBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				//Implement your edit logic for your module
				if (!Page.IsPostBack)
				{
					//get a list of users to assign the user to the Object
					ddlAssignedUser.DataSource = UserController.GetUsers(PortalId);
					ddlAssignedUser.DataTextField = "Username";
					ddlAssignedUser.DataValueField = "UserId";
					ddlAssignedUser.DataBind();

					//check if we have an ID passed in via a querystring parameter, if so, load that item to edit.
					//ItemId is defined in the ItemModuleBase.cs file
					if (ItemId > 0)
					{
						var tc = new FormController();

						var t = tc.GetForm(ItemId, ModuleId);
						if (t != null)
						{
							txtName.Text = t.FormName;
							//txtDescription.Text = t.ItemDescription;
							//ddlAssignedUser.Items.FindByValue(t.AssignedUserId.ToString()).Selected = true;
						}
					}
				}
			}
			catch (Exception exc) //Module failed to load
			{
				Exceptions.ProcessModuleLoadException(this, exc);
			}
		}


		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			var t = new Form();
			var tc = new FormController();

			if (ItemId > 0)
			{
				t = tc.GetForm(ItemId, ModuleId);
				t.FormName = txtName.Text.Trim();
				//t.FormDescription = txtDescription.Text.Trim();
				t.LastModifiedByUserId = UserId;
				t.LastModifiedOnDate = DateTime.Now;
				//t.AssignedUserId = Convert.ToInt32(ddlAssignedUser.SelectedValue);
			}
			else
			{
				t = new Form()
				{
					//AssignedUserId = Convert.ToInt32(ddlAssignedUser.SelectedValue),
					CreatedByUserId = UserId,
					CreatedOnDate = DateTime.Now,
					FormName = txtName.Text.Trim(),
					//ItemDescription = txtDescription.Text.Trim(),

				};
			}

			t.LastModifiedOnDate = DateTime.Now;
			t.LastModifiedByUserId = UserId;

			if (t.FormId > 0)
			{
				tc.UpdateForm(t);
			}
			else
			{
				tc.CreateForm(t);
			}
			Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
		}
	}
}