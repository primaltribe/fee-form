﻿using System;
using DotNetNuke.Entities.Modules;

namespace PTI.Modules.CWB
{
	public class CWBModuleBase : PortalModuleBase
	{
		public int ItemId
		{
			get
			{
				var qs = Request.QueryString["tid"];
				if (qs != null)
					return Convert.ToInt32(qs);
				return -1;
			}

		}
	}
}