﻿using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;

namespace PTI.Modules.CWB.Components
{
	[TableName("CWB_Fee_Forms")]
	//setup the primary key for table
	[PrimaryKey("FormId", AutoIncrement = true)]
	//configure caching using PetaPoco
	[Cacheable("Forms", CacheItemPriority.Default, 20)]
	//scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
	[Scope("CreatedByUserId")]
	class Form
	{
		///<summary>
		/// The ID of your object with the name of the FormName
		///</summary>
		public int FormId { get; set; }
		///<summary>
		/// A string with the name of the FormName
		///</summary>
		public string FormName { get; set; }

		///<summary>
		/// An integer for the user id of the user who created the object
		///</summary>
		public int CreatedByUserId { get; set; }

		///<summary>
		/// An integer for the user id of the user who last updated the object
		///</summary>
		public int LastModifiedByUserId { get; set; }

		///<summary>
		/// The date the object was created
		///</summary>
		public DateTime CreatedOnDate { get; set; }

		///<summary>
		/// The date the object was updated
		///</summary>
		public DateTime LastModifiedOnDate { get; set; }
	}
}
