﻿using System.Collections.Generic;
using DotNetNuke.Data;

namespace PTI.Modules.CWB.Components
{
	class AccountController
	{
		public void CreateAccount(Account t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				rep.Insert(t);
			}
		}

		public void DeleteAccount(int accountId)
		{
			var t = GetAccount(accountId);
			DeleteAccount(t);
		}

		public void DeleteAccount(Account t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				rep.Delete(t);
			}
		}

		public IEnumerable<Account> GetAccounts(int formId)
		{
			IEnumerable<Account> t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				t = rep.Get(formId);
			}
			return t;
		}

		public Account GetAccount(int accountId, int formId)
		{
			Account t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				t = rep.GetById(accountId, formId);
			}
			return t;
		}

		public Account GetAccount(int accountId)
		{
			Account t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				t = rep.GetById(accountId);
			}
			return t;
		}

		public void UpdateAccount(Account t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Account>();
				rep.Update(t);
			}
		}
	}
}
