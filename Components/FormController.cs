﻿using System.Collections.Generic;
using DotNetNuke.Data;

namespace PTI.Modules.CWB.Components
{
	class FormController
	{
		public void CreateForm(Form t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				rep.Insert(t);
			}
		}

		public void DeleteForm(int formId, int userId)
		{
			var t = GetForm(formId, userId);
			DeleteForm(t);
		}

		public void DeleteForm(Form t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				rep.Delete(t);
			}
		}

		public IEnumerable<Form> GetForms(int userId)
		{
			IEnumerable<Form> t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				t = rep.Get(userId);
			}
			return t;
		}

		public IEnumerable<Form> GetForms()
		{
			IEnumerable<Form> t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				t = rep.Get();
			}
			return t;
		}

		public Form GetForm(int formId, int userId)
		{
			Form t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				t = rep.GetById(formId, userId);
			}
			return t;
		}

		public Form GetForm(int formId)
		{
			Form t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				t = rep.GetById(formId);
			}
			return t;
		}

		public void UpdateForm(Form t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Form>();
				rep.Update(t);
			}
		}

	}
}
