﻿using System.Collections.Generic;
using DotNetNuke.Data;

namespace PTI.Modules.CWB.Components
{
	class EntryController
	{
		public void CreateEntry(Entry t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				rep.Insert(t);
			}
		}

		public void DeleteEntry(int accountId)
		{
			var t = GetEntry(accountId);
			DeleteEntry(t);
		}

		public void DeleteEntry(Entry t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				rep.Delete(t);
			}
		}

		public IEnumerable<Entry> GetEntries(int formId)
		{
			IEnumerable<Entry> t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				t = rep.Get(formId);
			}
			return t;
		}

		public Entry GetEntry(int accountId, int formId)
		{
			Entry t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				t = rep.GetById(accountId, formId);
			}
			return t;
		}


		public Entry GetEntry(int accountId)
		{
			Entry t;
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				t = rep.GetById(accountId);
			}
			return t;
		}

		public void UpdateAccount(Entry t)
		{
			using (IDataContext ctx = DataContext.Instance())
			{
				var rep = ctx.GetRepository<Entry>();
				rep.Update(t);
			}
		}

	}
}
