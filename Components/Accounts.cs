﻿using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;

namespace PTI.Modules.CWB.Components
{
	[TableName("CWB_Fee_Accounts")]
	//setup the primary key for table
	[PrimaryKey("AccountId", AutoIncrement = true)]
	//configure caching using PetaPoco
	[Cacheable("Accounts", CacheItemPriority.Default, 0)]
	//scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
	[Scope("FormId")]
	class Account
	{
		///<summary>
		/// The ID of your object with the name of the AccountName
		///</summary>
		public int AccountId { get; set; }
		///<summary>
		/// A string with the name of the FormName
		///</summary>
		public string AccountName { get; set; }

		///<summary>
		/// A decimal with the name of the AccountPerItemFee
		///</summary>
		public decimal AccountPerItemFee { get; set; }

		///<summary>
		/// A decimal with the name of the AccountMinMonthFee
		///</summary>
		public decimal AccountMinMonthFee { get; set; }

		///<summary>
		/// The FormId of where the object was created and gets displayed
		///</summary>
		public int FormId { get; set; }

		///<summary>
		/// An integer for the user id of the user who created the object
		///</summary>
		public int CreatedByUserId { get; set; }

		///<summary>
		/// An integer for the user id of the user who last updated the object
		///</summary>
		public int LastModifiedByUserId { get; set; }

		///<summary>
		/// The date the object was created
		///</summary>
		public DateTime CreatedOnDate { get; set; }

		///<summary>
		/// The date the object was updated
		///</summary>
		public DateTime LastModifiedOnDate { get; set; }

		public Account()
		{
			this.CreatedByUserId = 1;
			this.LastModifiedByUserId = 1;
			this.CreatedOnDate = DateTime.Now;
			this.LastModifiedOnDate = DateTime.Now;
		}
	}
}
