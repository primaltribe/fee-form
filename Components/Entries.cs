﻿using System;
using System.Web.Caching;
using DotNetNuke.Common.Utilities;
using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Content;

namespace PTI.Modules.CWB.Components
{
	[TableName("CWB_Fee_Entries")]
	//setup the primary key for table
	[PrimaryKey("AccountId", AutoIncrement = false)]
	//configure caching using PetaPoco
	[Cacheable("Entry", CacheItemPriority.Default, 20)]
	//scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
	[Scope("FormId")]
	class Entry
	{
		///<summary>
		/// The ModuleId of where the object was created and gets displayed
		///</summary>
		public int AccountId { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Deposits1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Deposits2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Deposits3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Bills1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Bills2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Bills3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Rolls1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Rolls2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Rolls3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Boxes1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Boxes2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Boxes3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesDeposited1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesDeposited2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesDeposited3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cash1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cash2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cash3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal FBPS1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal FBPS2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal FBPS3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDeposited1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDeposited2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDeposited3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDebit1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDebit2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSDebit3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSCredit1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSCredit2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal NDDSCredit3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cheques1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cheques2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cheques3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTCredits1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTCredits2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTCredits3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTDebits1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTDebits2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal EFTDebits3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal CWB1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal CWB2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal CWB3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal IDP1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal IDP2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal IDP3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMDeposits1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMDeposits2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMDeposits3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMWithdrawls1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMWithdrawls2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ABMWithdrawls3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cirrus1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cirrus2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Cirrus3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Drafts1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Drafts2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Drafts3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Utility1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Utility2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal Utility3 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesCertified1 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesCertified2 { get; set; }

		///<summary>
		/// A decimal with the name of the 
		///</summary>
		public decimal ChequesCertified3 { get; set; }

		///<summary>
		/// The ModuleId of where the object was created and gets displayed
		///</summary>
		public int FormId { get; set; }

		///<summary>
		/// An integer for the user id of the user who created the object
		///</summary>
		public int CreatedByUserId { get; set; }

		///<summary>
		/// An integer for the user id of the user who last updated the object
		///</summary>
		public int LastModifiedByUserId { get; set; }

		///<summary>
		/// The date the object was created
		///</summary>
		public DateTime CreatedOnDate { get; set; }

		///<summary>
		/// The date the object was updated
		///</summary>
		public DateTime LastModifiedOnDate { get; set; }

		public Entry()
		{
			this.CreatedByUserId = 1;
			this.LastModifiedByUserId = 1;
			this.CreatedOnDate = DateTime.Now;
			this.LastModifiedOnDate = DateTime.Now;
		}
	}
}
