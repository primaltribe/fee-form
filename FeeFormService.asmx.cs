﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using Newtonsoft.Json;
using PTI.Modules.CWB.Components;

namespace PTI.Modules.CWB
{
	/// <summary>
	/// Summary description for FeeFormService
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
	public class FeeFormService : System.Web.Services.WebService
	{
		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public void SaveMutipleGridChanges(string changesStr, string formIdStr)
		{
			string changes = JsonConvert.DeserializeObject(changesStr).ToString();
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return;

			string[] changePartialArray = changes.Split(',');
			List<List<string>> changeArray = new List<List<string>>();

			for (int i = 0; i < changePartialArray.Count(); i=i+4 )
			{
				List<string> changeInnerArray = new List<string>();
				changeInnerArray.Add(changePartialArray[i]);
				changeInnerArray.Add(changePartialArray[i+1]);
				changeInnerArray.Add(changePartialArray[i+2]);
				changeInnerArray.Add(changePartialArray[i+3]);

				changeArray.Add(changeInnerArray);
			}

			AccountController ac = new AccountController();
			System.Collections.Generic.List<Account> accounts = ac.GetAccounts(formId).ToList<Account>();

			foreach(List<string> change in changeArray)
			{
				int accountId = 0;
				accountId = accounts[int.Parse(change[1])/3].AccountId;

				SaveGridChange(accountId, int.Parse(change[0]), int.Parse(change[1])%3, decimal.Parse(change[3]));
			}
			updateFormDate(formId);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public void SaveGridChange(string accountIdStr, string rowStr, string columnStr, string valueStr)
		{
			int accountId = int.Parse(JsonConvert.DeserializeObject(accountIdStr).ToString());
			int row = int.Parse(JsonConvert.DeserializeObject(rowStr).ToString());
			int column = int.Parse(JsonConvert.DeserializeObject(columnStr).ToString());
			decimal value = decimal.Parse(JsonConvert.DeserializeObject(valueStr).ToString());

			if (!canEditAccount(accountId))
				return;

			SaveGridChange(accountId, row, column, value);
			upateFormDateByAccount(accountId);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public void UpdateForm(string formIdStr, string formNameStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());
			string formName = JsonConvert.DeserializeObject(formNameStr).ToString();

			if (!canEditForm(formId))
				return;

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			FormController fc = new FormController();
			Form form = fc.GetForm(formId, userinfo.UserID);
			form.FormName = formName;
			form.LastModifiedOnDate = DateTime.Now;
			form.LastModifiedByUserId = userinfo.UserID;
			fc.UpdateForm(form);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public void DeleteForm(string formIdStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return;

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			EntryController ec = new EntryController();
			System.Collections.Generic.List<Entry> entryList = ec.GetEntries(formId).ToList<Entry>();
			foreach (Entry entry in entryList)
			{
				ec.DeleteEntry(entry);
			}

			AccountController ac = new AccountController();
			System.Collections.Generic.List<Account> accountList = ac.GetAccounts(formId).ToList<Account>();
			foreach (Account account in accountList)
			{
				ac.DeleteAccount(account);
			}

			FormController fc = new FormController();
			Form form = fc.GetForm(formId);
			fc.DeleteForm(form);

			return;
		}

		public void SaveGridChange(int accountId, int row, int column, decimal value)
		{
			EntryController ec = new EntryController();
			Entry entry = ec.GetEntry(accountId);
			entry.LastModifiedOnDate = DateTime.Now;
			switch (row)
			{
				case 0:
					if(column == 0)
						entry.Deposits1 = value;
					else if(column == 1)
						entry.Deposits2 = value;
					else if(column == 2)
						entry.Deposits3 = value;
					break;
				case 1:
					if (column == 0)
						entry.Bills1 = value;
					else if (column == 1)
						entry.Bills2 = value;
					else if (column == 2)
						entry.Bills3 = value;
					break;
				case 2:
					if (column == 0)
						entry.Rolls1 = value;
					else if (column == 1)
						entry.Rolls2 = value;
					else if (column == 2)
						entry.Rolls3 = value;
					break;
				case 3:
					if (column == 0)
						entry.Boxes1 = value;
					else if (column == 1)
						entry.Boxes2 = value;
					else if (column == 2)
						entry.Boxes3 = value;
					break;
				case 4:
					if (column == 0)
						entry.ChequesDeposited1 = value;
					else if (column == 1)
						entry.ChequesDeposited2 = value;
					else if (column == 2)
						entry.ChequesDeposited3 = value;
					break;
				case 5:
					if (column == 0)
						entry.Cash1 = value;
					else if (column == 1)
						entry.Cash2 = value;
					else if (column == 2)
						entry.Cash3 = value;
					break;
				case 6:
					if (column == 0)
						entry.FBPS1 = value;
					else if (column == 1)
						entry.FBPS2 = value;
					else if (column == 2)
						entry.FBPS3 = value;
					break;
				case 7:
					if (column == 0)
						entry.NDDSDeposited1 = value;
					else if (column == 1)
						entry.NDDSDeposited2 = value;
					else if (column == 2)
						entry.NDDSDeposited3 = value;
					break;
				case 8:
					if (column == 0)
						entry.NDDSDebit1 = value;
					else if (column == 1)
						entry.NDDSDebit2 = value;
					else if (column == 2)
						entry.NDDSDebit3 = value;
					break;
				case 9:
					if (column == 0)
						entry.NDDSCredit1 = value;
					else if (column == 1)
						entry.NDDSCredit2 = value;
					else if (column == 2)
						entry.NDDSCredit3 = value;
					break;
				case 10:
					if (column == 0)
						entry.Cheques1 = value;
					else if (column == 1)
						entry.Cheques2 = value;
					else if (column == 2)
						entry.Cheques3 = value;
					break;
				case 11:
					if (column == 0)
						entry.EFTCredits1 = value;
					else if (column == 1)
						entry.EFTCredits2 = value;
					else if (column == 2)
						entry.EFTCredits3 = value;
					break;
				case 12:
					if (column == 0)
						entry.EFTDebits1 = value;
					else if (column == 1)
						entry.EFTDebits2 = value;
					else if (column == 2)
						entry.EFTDebits3 = value;
					break;
				case 13:
					if (column == 0)
						entry.CWB1 = value;
					else if (column == 1)
						entry.CWB2 = value;
					else if (column == 2)
						entry.CWB3 = value;
					break;
				case 14:
					if (column == 0)
						entry.IDP1 = value;
					else if (column == 1)
						entry.IDP2 = value;
					else if (column == 2)
						entry.IDP3 = value;
					break;
				case 15:
					if (column == 0)
						entry.ABMDeposits1 = value;
					else if (column == 1)
						entry.ABMDeposits2 = value;
					else if (column == 2)
						entry.ABMDeposits3 = value;
					break;
				case 16:
					if (column == 0)
						entry.ABMWithdrawls1 = value;
					else if (column == 1)
						entry.ABMWithdrawls2 = value;
					else if (column == 2)
						entry.ABMWithdrawls3 = value;
					break;
				case 17:
					if (column == 0)
						entry.Cirrus1 = value;
					else if (column == 1)
						entry.Cirrus2 = value;
					else if (column == 2)
						entry.Cirrus3 = value;
					break;
				case 18:
					if (column == 0)
						entry.Drafts1 = value;
					else if (column == 1)
						entry.Drafts2 = value;
					else if (column == 2)
						entry.Drafts3 = value;
					break;
				case 19:
					if (column == 0)
						entry.Utility1 = value;
					else if (column == 1)
						entry.Utility2 = value;
					else if (column == 2)
						entry.Utility3 = value;
					break;
				case 20:
					if (column == 0)
						entry.ChequesCertified1 = value;
					else if (column == 1)
						entry.ChequesCertified2 = value;
					else if (column == 2)
						entry.ChequesCertified3 = value;
					break;
			}

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();
			entry.LastModifiedByUserId = userinfo.UserID;
			ec.UpdateAccount(entry);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public int CreateAccount(string formIdStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return -1;

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			//Create new account for form
			AccountController ac = new AccountController();
			Account account = new Account();
			account.FormId = formId;
			account.CreatedByUserId = userinfo.UserID;
			account.LastModifiedByUserId = userinfo.UserID;
			ac.CreateAccount(account);

			//Add an entry so the grid will populate correctly
			EntryController ec = new EntryController();
			Entry entry = new Entry();
			entry.AccountId = account.AccountId;
			entry.FormId = account.FormId;
			entry.CreatedByUserId = userinfo.UserID;
			entry.LastModifiedByUserId = userinfo.UserID;
			ec.CreateEntry(entry);

			updateFormDate(formId);

			return account.AccountId;
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public int SaveAccount(string accountIdStr, string nameStr, string perItemStr, string minMonthStr)
		{
			int accountId = int.Parse(JsonConvert.DeserializeObject(accountIdStr).ToString());
			string name = JsonConvert.DeserializeObject(nameStr).ToString();
			decimal perItem = 0.0M;
			if (!string.IsNullOrEmpty(JsonConvert.DeserializeObject(perItemStr).ToString()))
				perItem = decimal.Parse(JsonConvert.DeserializeObject(perItemStr).ToString());
			decimal minMonth = 0.0M;
			if (!string.IsNullOrEmpty(JsonConvert.DeserializeObject(minMonthStr).ToString()))
				minMonth = decimal.Parse(JsonConvert.DeserializeObject(minMonthStr).ToString());

			if (!canEditAccount(accountId))
				return -1;

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			//Create new account for form
			AccountController ac = new AccountController();
			Account account = ac.GetAccount(accountId);
			account.AccountName = name;
			account.AccountPerItemFee = perItem;
			account.AccountMinMonthFee = minMonth;
			account.LastModifiedByUserId = userinfo.UserID;
			account.CreatedByUserId = userinfo.UserID;
			ac.UpdateAccount(account);

			upateFormDateByAccount(accountId);

			return account.AccountId;
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetAccounts(string formIdStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return "";

			AccountController ac = new AccountController();
			System.Collections.Generic.List<Account> accounts = ac.GetAccounts(formId).ToList<Account>();

			updateFormDate(formId);

			return JsonConvert.SerializeObject(accounts);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public void DeleteAccount(string accountIdStr)
		{
			int accountId = int.Parse(JsonConvert.DeserializeObject(accountIdStr).ToString());

			if (!canEditAccount(accountId))
				return;

			//Create new account for form
			AccountController ac = new AccountController();
			Account account = ac.GetAccount(accountId);
			ac.DeleteAccount(accountId);
			EntryController ec = new EntryController();
			ec.DeleteEntry(accountId);
			
			updateFormDate(account.FormId);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetEntries(string formIdStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return "";

			EntryController ec = new EntryController();
			System.Collections.Generic.List<Entry> entryList = ec.GetEntries(formId).ToList<Entry>();

			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter(sb);

			using (JsonWriter writer = new JsonTextWriter(sw))
			{

				writer.WriteStartObject();
				writer.WritePropertyName("data");
				writer.WriteStartArray();

				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Deposits1);
					writer.WriteValue(entry.Deposits2);
					writer.WriteValue(entry.Deposits3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Bills1);
					writer.WriteValue(entry.Bills2);
					writer.WriteValue(entry.Bills3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Rolls1);
					writer.WriteValue(entry.Rolls2);
					writer.WriteValue(entry.Rolls3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Boxes1);
					writer.WriteValue(entry.Boxes2);
					writer.WriteValue(entry.Boxes3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.ChequesDeposited1);
					writer.WriteValue(entry.ChequesDeposited2);
					writer.WriteValue(entry.ChequesDeposited3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Cash1);
					writer.WriteValue(entry.Cash2);
					writer.WriteValue(entry.Cash3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.FBPS1);
					writer.WriteValue(entry.FBPS2);
					writer.WriteValue(entry.FBPS3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.NDDSDeposited1);
					writer.WriteValue(entry.NDDSDeposited2);
					writer.WriteValue(entry.NDDSDeposited3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.NDDSDebit1);
					writer.WriteValue(entry.NDDSDebit2);
					writer.WriteValue(entry.NDDSDebit3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.NDDSCredit1);
					writer.WriteValue(entry.NDDSCredit2);
					writer.WriteValue(entry.NDDSCredit3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Cheques1);
					writer.WriteValue(entry.Cheques2);
					writer.WriteValue(entry.Cheques3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.EFTCredits1);
					writer.WriteValue(entry.EFTCredits2);
					writer.WriteValue(entry.EFTCredits3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.EFTDebits1);
					writer.WriteValue(entry.EFTDebits2);
					writer.WriteValue(entry.EFTDebits3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.CWB1);
					writer.WriteValue(entry.CWB2);
					writer.WriteValue(entry.CWB3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.IDP1);
					writer.WriteValue(entry.IDP2);
					writer.WriteValue(entry.IDP3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.ABMDeposits1);
					writer.WriteValue(entry.ABMDeposits2);
					writer.WriteValue(entry.ABMDeposits3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.ABMWithdrawls1);
					writer.WriteValue(entry.ABMWithdrawls2);
					writer.WriteValue(entry.ABMWithdrawls3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Cirrus1);
					writer.WriteValue(entry.Cirrus2);
					writer.WriteValue(entry.Cirrus3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Drafts1);
					writer.WriteValue(entry.Drafts2);
					writer.WriteValue(entry.Drafts3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.Utility1);
					writer.WriteValue(entry.Utility2);
					writer.WriteValue(entry.Utility3);
				}
				writer.WriteEndArray();
				writer.WriteStartArray();
				foreach (Entry entry in entryList)
				{
					writer.WriteValue(entry.ChequesCertified1);
					writer.WriteValue(entry.ChequesCertified2);
					writer.WriteValue(entry.ChequesCertified3);
				}
				writer.WriteEndArray();
				writer.WriteEnd();
				writer.WriteEndObject();
			}
			
			return sb.ToString();
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetForms()
		{
			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId,HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;

			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			FormController fc = new FormController();
			System.Collections.Generic.List<Form> formList;

			if(userinfo.IsSuperUser)
				formList = fc.GetForms().ToList<Form>();
			else
				formList = fc.GetForms(userinfo.UserID).ToList<Form>();

			return JsonConvert.SerializeObject(formList);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetForm(string formIdStr)
		{
			int formId = int.Parse(JsonConvert.DeserializeObject(formIdStr).ToString());

			if (!canEditForm(formId))
				return "";

			FormController fc = new FormController();

			return JsonConvert.SerializeObject(fc.GetForm(formId));
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public int CreateForm(string formNameStr)
		{
			string formName = JsonConvert.DeserializeObject(formNameStr).ToString();

			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			FormController fc = new FormController();
			Form form = new Form();
			form.FormName = formName;
			form.CreatedByUserId = userinfo.UserID;
			form.CreatedOnDate = DateTime.Now;
			form.LastModifiedByUserId = userinfo.UserID;
			form.LastModifiedOnDate = DateTime.Now;
			fc.CreateForm(form);

			return form.FormId;
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetAccount(string accountIdStr)
		{
			int accountId = int.Parse(JsonConvert.DeserializeObject(accountIdStr).ToString());

			if (!canEditAccount(accountId))
				return "";

			AccountController ac = new AccountController();
			Account account = ac.GetAccount(accountId);

			return JsonConvert.SerializeObject(account);
		}

		[ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
		public string GetEntry(string accountIdStr)
		{
			int accountId = int.Parse(JsonConvert.DeserializeObject(accountIdStr).ToString());

			if (!canEditAccount(accountId))
				return "";

			EntryController ec = new EntryController();
			Entry entry = ec.GetEntry(accountId);
			return JsonConvert.SerializeObject(entry);
		}

		protected Boolean canEditAccount(int accountId)
		{
			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			AccountController ac = new AccountController();
			Account account = ac.GetAccount(accountId);

			return account.CreatedByUserId == userinfo.UserID || userinfo.IsSuperUser;
		}

		protected Boolean canEditForm(int formId)
		{
			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			FormController fc = new FormController();
			Form form = fc.GetForm(formId);

			return form.CreatedByUserId == userinfo.UserID || userinfo.IsSuperUser;
		}

		protected void updateFormDate(int formId)
		{
			UserInfo user = UserController.GetUserByName(PortalSettings.Current.PortalId, HttpContext.Current.User.Identity.Name);
			HttpContext.Current.Items["UserInfo"] = user;
			UserInfo userinfo = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

			FormController fc = new FormController();
			Form form = fc.GetForm(formId);
			form.LastModifiedOnDate = DateTime.Now;
			fc.UpdateForm(form);

			return;
		}
		
		protected void upateFormDateByAccount(int accountId)
		{
			AccountController ac = new AccountController();
			Account account = ac.GetAccount(accountId);
			updateFormDate(account.FormId);
			return;
		}
	}
}
